import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test.testexamples.KavosMasina;


import static org.junit.jupiter.api.Assertions.assertTrue;


public class KavosMasinaTest {
    @Test
    public void duotaUzpildytasKavosAparatas_kaiGaminamKava_tadaPamazejaProduktu() throws Exception {
        //Duota
        KavosMasina kavosMasina = new KavosMasina();
        kavosMasina.uzpildytiPupeles(100);
        kavosMasina.uzpildytiVandens(2);

        //Kai
        kavosMasina.darytiJuodaKava();

        //Tada
        assertTrue(kavosMasina.getVandensKiekis() == 1, () -> "blogai");
        assertTrue(kavosMasina.getPupeliuKiekis() == 80);
        //        if(kavosMasina.getVandensKiekis() == 1) {
//            System.out.println("Testas praejo");
//        } else {
//            throw new Exception("Klaida");
//        }
    }

    @Test
    public void duotasKavosAparatas_kaiTikrinamArUztenkaProduktuGamntiKava_taiTuriPranestiKadUztenka() {
        KavosMasina kavosMasina = new KavosMasina();
        kavosMasina.uzpildytiPupeles(100);
        kavosMasina.uzpildytiVandens(100);

        assertTrue(kavosMasina.tikrintiArUztenkaProduktu() == true);
    }

    /**
     * Kavos aparatas turi patikrinti ar užtenka produktų
     * prieš darydamas kavą.
     *
     *
     * Sąlygą - produktai negali nueiti į minusą.
     */

}
