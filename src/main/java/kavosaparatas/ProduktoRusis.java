package kavosaparatas;

public enum ProduktoRusis {
    KAVOS_MILTELIAI,
    KAVOS_PUPELES,
    VANDUO,
    CUKRUS,
    PIENAS
}
