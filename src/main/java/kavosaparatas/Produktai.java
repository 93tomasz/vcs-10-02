package kavosaparatas;

public class Produktai {
    int cukrus;
    int kava;
    int vanduo;

    public void pildytiCukru(int cukrausKiekis) {
        this.cukrus += cukrausKiekis;
    }

    public void naudotiCukru(int cukrausKiekis) {
        this.cukrus -= cukrausKiekis;
    }

    public void naudotiKava(int kavosKiekis) {
        this.kava -= kavosKiekis;
    }

    public Produktai kopijuoti() {

        Produktai kopija = new Produktai();
        kopija.cukrus = this.cukrus;
        kopija.kava = this.kava;
        kopija.vanduo = this.vanduo;
        return kopija;
    }
}
