package kavosaparatas;

public interface PuodelisInterface {
    /**
     * Metodas isplauna puodeli. Jeigu pavyksta isplauti - metodas grazina true.
     *
     * @return pavyko (true) ar nepavyko (false)
     */
    boolean isplauti();


    /**
     * 0 - kava
     * 1 - vanduo
     * 2 - cukrus
     *
     * @param produktoRusis
     * @param kiekis - ipilamas i puodeli
     */
    void pripildyti(int produktoRusis, int kiekis);

    /**
     * Puodelis tampa nebenaudojamas
     */
    void sulamdyti();

    /**
     * Ant puodelio atspausdinamas kavos pavadinimas
     *
     * @param pavadinimas nurodyta kavos rusis
     */
    void nurodytiKavosPavadinima(String pavadinimas);

}
