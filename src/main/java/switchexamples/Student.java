package switchexamples;

public class Student {
//    KavosRusys kavosRusis;
    double pazymiuVidurkis;
    String asmensKodas;

    private void privateMethod() {
        System.out.println("Privatus!");
    }

    void nomodifier() {
        System.out.println("Be pasiekiamumo lygio!");
    }

    protected void protectedMethod() {
        System.out.println("Apsaugotas!");
    }

    public void publicMethod() {
        System.out.println("Viesas!");
    }

    public void nustatytiPazymiuVidurki(int pazymys1, int pazymys2, int pazymys3) {
        pazymiuVidurkis = (pazymys1 + pazymys2 + pazymys3) / 3;
    }

//    public void nustatytiKoda(String asmensKodas) {
//        this.asmensKodas = asmensKodas;
//        kavosRusis = KavosRusys.BALTA;
//
//    }

    public static void sumuoti() {

    }

    public static int susumuok(int skaicius1, int skaicius2) {
        int suma;

        suma = skaicius1 + skaicius2;

        Student student = new Student();
        student.privateMethod();


        return suma;
    }

}
