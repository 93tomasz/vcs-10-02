import jpa.HibernateImplementation;
import jpa.Zaidejas;
import persistence.PersistenceProvider;

public class Application {
    public static void main(String[] args) throws Exception {
        PersistenceProvider hibernate = new PersistenceProvider();


        Zaidejas zaidejas = hibernate.getEntityManager().find(Zaidejas.class, 1);

        zaidejas.setName("Petras");
        zaidejas.setAmzius(24);

        hibernate.getEntityManager().getTransaction().begin();

        hibernate.getEntityManager().persist(zaidejas);

        hibernate.getEntityManager().getTransaction().commit();
        System.exit(0);
    }
}
