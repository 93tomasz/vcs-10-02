package persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceProvider {
    private EntityManagerFactory sessionFactory;
    private EntityManager entityManager;

    public PersistenceProvider() {
        sessionFactory = Persistence.createEntityManagerFactory( "org.hibernate.tutorial.jpa" );
        entityManager = sessionFactory.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
