package tekstai;

import switchexamples.Student;

public class TekstoSkaidymas extends Student {
    public static void main(String[] args) {

        String textWithWhitespaces = "labas rytas as velavau";
        // skaidymas per tarpus naudojant split
        String[] textArray = textWithWhitespaces.split("\\s+");

        for (String text: textArray) {
            System.out.println(text);
        }

        // skaidymas per tarpus su indexOf
//        int index = 0;
//        while (index != -1) {
//            int previousIndex = index + 1;
//            index = textWithWhitespaces.indexOf(" ", previousIndex);
//            System.out.println(index);
//            System.out.println(textWithWhitespaces.substring(previousIndex, index));
//
//        }


        // skaidymas per tarpus dedant i masyva
        String array[] = new String[10];
        int wordIndex = 0;
        int symbolCount = textWithWhitespaces.length();
        array[wordIndex] = "";
        for(int i = 0; i<symbolCount; i++) {
            char currentChar = textWithWhitespaces.charAt(i);
            if(currentChar == ' ') {
                wordIndex++;
                array[wordIndex] = "";
            } else {
                array[wordIndex] = array[wordIndex].concat("" + currentChar);
            }
        }

        // masyvo spausdinimas be null elementu
        for (String text: array) {
            if(text != null) {
                System.out.println(text);
            }
        }

        // Be masyvo
        for(int i = 0; i < textWithWhitespaces.length(); i++) {
            char currentChar = textWithWhitespaces.charAt(i);
            if(currentChar == ' ') {
                System.out.println();
            } else {
                System.out.print("" + currentChar);
            }
        }
    }
}