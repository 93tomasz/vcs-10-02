package exceptions;

public class KavosAparatas {
    private int vandensKiekis = 1000;
    private int puodeliuKiekis = 1000;
    private Integer skaicius;

    private boolean maintenanceMode = false;

    public void makeCoffee() throws InterruptedException {
        System.out.println("Padedu puodeli");
        System.out.println("pradedu pilti...");
        Thread.sleep(1000);
        System.out.print("..");
        Thread.sleep(1000);
        System.out.print("..");
        Thread.sleep(1000);
        System.out.print("..");
        System.out.println();
        try {
            naudotiPuodeli();
            naudotiVandeni(100);
        } catch (ConsumablesException e) {
            if(!e.vanduo) System.out.println("truksta vandens");
            if(!e.pienas) System.out.println("truksta pieno");
            if(!e.kava) System.out.println("truksta kava");
            if(!e.cukrus) System.out.println("truksta cukraus");
        } catch (Exception e) {
            System.out.println("Kazkas ivyko blogai.");
        } finally {

        }

    }

    private void naudotiPuodeli() throws Exception {
        if(puodeliuKiekis < 1) {
            throw new ConsumablesException(true, true, false, false);
        }
        puodeliuKiekis --;
    }

    public void naudotiVandeni(int kiekis) throws Exception {
        if(vandensKiekis < kiekis) {
            throw new NoWaterException("Truksta vandens: " + (kiekis - vandensKiekis));
        }
        vandensKiekis -= kiekis;
    }
}
