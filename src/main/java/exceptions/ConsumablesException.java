package exceptions;

public class ConsumablesException extends Exception {
    public final boolean cukrus;
    public final boolean pienas;
    public final boolean vanduo;
    public final boolean kava;

    public ConsumablesException(String s) {
        super(s);
        this.cukrus = false;
        this.kava = false;
        this.pienas = false;
        this.vanduo = false;
    }

    public ConsumablesException(boolean cukrus, boolean kava, boolean vanduo, boolean pienas) {
        super("Empty consumables");
        this.cukrus = cukrus;
        this.kava = kava;
        this.vanduo = vanduo;
        this.pienas = pienas;
    }
}
