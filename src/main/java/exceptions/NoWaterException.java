package exceptions;

public class NoWaterException extends Exception {
    public NoWaterException(String s) {
        super(s);
    }
}
