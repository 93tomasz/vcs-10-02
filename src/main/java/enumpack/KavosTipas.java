package enumpack;

public enum KavosTipas {
    JUODA("juoda", 3, 2, 1),
    LATTE("latte",2,2,2),
    SU_PIENU("su pienu",2,2,2),
    BALTA("balta", 1,2,3);

    public final String rusis;
    public final int vanduo;
    public final int pienas;
    public final int cukrus;


    KavosTipas(String rusis, int cukrus, int pienas, int vanduo) {
        this.rusis = rusis;
        this.cukrus = cukrus;
        this.pienas = pienas;
        this.vanduo = vanduo;
    }

    public static KavosTipas pagalRusi(String rusis) {
        for(KavosTipas tipas : KavosTipas.values()) {
            if(tipas.rusis.equals(rusis)) {
                return tipas;
            }
        }
        return KavosTipas.JUODA;
    }
}
