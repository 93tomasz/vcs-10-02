package classdepth;

public class Pirma {
    private Antra antrasObjektas;

    /*
    Kuriam antra klase, sukurta objekta priskiriam 'antrasObjektas'
    kintamajam.
    \_
      \_
        \_
          |
        _/
     _/
    /

     */
    public Pirma() {
        antrasObjektas = new Antra();
    }

    public void liepkRektSeptintamObjektui() {
        antrasObjektas.liepk();
    }

//    public void imkFanta(Fantas fantas) {

//    }
}
