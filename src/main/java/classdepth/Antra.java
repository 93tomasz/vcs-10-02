package classdepth;

public class Antra {
    private Trecia trecias;
    private int gitKintamasis;
    private String busimasTekstas;

    public Antra() {
        this(new Trecia(), 55);
        busimasTekstas = "labas rytas";
    }

    public Antra(Trecia trecias, int gitKintamasis) {
        this.trecias = trecias;
        this.gitKintamasis = gitKintamasis;
    }

    public int liepk() {
        gitKintamasis = 45;
        darykKazka("spausdink sita teksta");
//        trecias.liepkRektSeptintamObjektui(gitKintamasis);
        return 45;
    }

    private int atliktiSkaiciavimus(int skaicius1, int skaicius2) {
        return skaicius1 + skaicius2 * gitKintamasis;
    }

    public int darykKazka(String tekstas) {
        System.out.println(tekstas);
        return 10;
    }

    public Trecia getTrecias() {
        return trecias;
    }

    public void setTrecias(Trecia trecias) {
        this.trecias = trecias;
    }
}
