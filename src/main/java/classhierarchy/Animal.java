package classhierarchy;

public abstract class Animal {
    int prisikaupe = 2;

    public double averageSpeed() {
        return 0.56;
    }

    public abstract void move();

    protected void esti(int maistas) {
        kaupimas();
        maistas --;
    }

    private void kaupimas() {
        prisikaupe ++;
    }

    public void skleistiGarsa() {
        System.out.println("WOOOOOP!!!");
    }
}
