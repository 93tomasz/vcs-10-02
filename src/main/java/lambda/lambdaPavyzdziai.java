package lambda;

import java.util.*;
import java.util.stream.Collectors;

class Person {

    public Person(String name, Long phone, String lastname, String address, boolean convicted) {
        this.name = name;
        this.phone = phone;
        this.lastname = lastname;
        this.address = address;
        this.convicted = convicted;
    }

    String name;
    Long phone;
    String lastname;
    String address;
    boolean convicted;

    public String getName() {
        return name;
    }

    public Long getPhone() {
        return phone;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }

    public boolean isConvicted() {
        return convicted;
    }
}

public class lambdaPavyzdziai {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            list.add(3*i);
        }

        for (Integer element : list){
            System.out.println(element);
        }

        List<String> tekstuSarasas= new ArrayList<>();
        tekstuSarasas.add("pirmasis");
        tekstuSarasas.add("antrasis");
        tekstuSarasas.add("karas ir taika");

        String[] tekstuMasyvas = new String[3];
        tekstuMasyvas[0] = "pirmasis";
        tekstuMasyvas[1] = "antrasis";
        tekstuMasyvas[2] = "karas ir taika";

        tekstuSarasas.add("labas");
        tekstuSarasas.add("ate");

        for (String masyvoElementas : tekstuMasyvas) {

        }

        for (String sarasoElementas : tekstuSarasas) {
            System.out.println(sarasoElementas);
        }

        Map<String, Integer> mapas = new HashMap<>();

        mapas.put("labas", 1);
        mapas.put("ate", 2);

        for (Map.Entry<String, Integer> element : mapas.entrySet())
        {
            System.out.println(element.getKey());
            System.out.println(element.getValue());
        }


        list.forEach(irasas -> {
            irasas.doubleValue();
        });
        list.stream()
                .filter(element -> element != 1)
                .collect(Collectors.toList());

        mapas.forEach((raktas, reiksme) -> {
            System.out.println(raktas);
            System.out.println(reiksme);
        });


        List<Person> listOfPersons =
                Arrays.asList(
                        new Person("Tomas", 123L, "Zvaliauskas", "Vilnius, dasda", true),

                        new Person("Michail", 124L, "asdasdasd", "Vilnius, dasda", true),

                        new Person("Nikolajus", 125L, "vcxbnvbnv", "Vilnius, dasda", true),

                        new Person("Benas", 126L, "gfhfgjiuyi", "Alytus, dasda", false));

        Map<Long, String> wanteds = listOfPersons.stream()
                .filter(prs -> prs.isConvicted())
                .collect(
                        Collectors.toMap(
                                person -> person.getPhone(),
                                person -> person.getName()));


    }
}
