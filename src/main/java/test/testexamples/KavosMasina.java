package test.testexamples;

public class KavosMasina {

    private Integer vandensKiekis = 0;
    private Integer pupeliuKiekis = 0;

    public void uzpildytiPupeles(int pupeliuKiekis) {
        this.pupeliuKiekis += pupeliuKiekis;
    }

    public void uzpildytiVandens(int vandensKiekis) {
        this.vandensKiekis += vandensKiekis;
    }

    public void darytiJuodaKava() {
        vandensKiekis -= 1;
        pupeliuKiekis -= 20;
    }

    public Integer getVandensKiekis() {
        return vandensKiekis;
    }

    public void setVandensKiekis(Integer vandensKiekis) {
        vandensKiekis = vandensKiekis;
    }

    public Integer getPupeliuKiekis() {
        return pupeliuKiekis;
    }

    public void setPupeliuKiekis(Integer pupeliuKiekis) {
        this.pupeliuKiekis = pupeliuKiekis;
    }

    public boolean tikrintiArUztenkaProduktu() {
        boolean arUztenka = pupeliuKiekis > 20 && vandensKiekis > 1;

        return arUztenka;
    }
}
