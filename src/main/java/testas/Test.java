package testas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {
        System.out.println("Uzduotis1: \n");
        // 1
        int[] masyvas = {8, 9, 5, 8, 6, 7 };
        for(int i = masyvas.length -1; i >= 0; i--) {
            System.out.println(masyvas[i]);
        }

        System.out.println("Uzduotis2: \n");
        // 2
        int[][] masyvas2 = {{4, 5, 6}, {7,9,8}, {8,2,1}};
        for(int i = masyvas2.length -1; i >= 0; i--) {
            System.out.println(masyvas2[i][i]);
        }

        System.out.println("Uzduotis3: \n");
        // 3
        int[][]masyvas3 = {{4,5,6}, {7,9,8}, {8,2,1,4}};
        for(int i = masyvas3.length - 1; i >= 0; i--) {
            for(int k = 0; k < masyvas3[i].length; k ++) {
                System.out.println(masyvas3[i][k]);
            }
        }
        System.out.println("Uzduotis4: \n");
        //4
        int[][] masyvas4 = {{4,5,6}, {8,2,1,4}};
        for(int i = 0; i<masyvas4.length; i++) {
            for(int k = 0; k< masyvas4[i].length; k++) {
                System.out.println(masyvas4[i][k]);
            }
        }

        System.out.println("Uzduotis5: \n");
        //5
        int[][] masyvas5 = {{4,5,6}};
        int[][] masyvas6 = {{4,5,6}};
        if(masyvas5 == masyvas6) {
            System.out.println("Lygus");
        } else {
            System.out.println("Nelygus");
        }

        System.out.println("Uzduotis6: \n");
        // 6
        int[][] masyvas7 = {{4,5,6}};
        int[][] masyvas8 = masyvas7;
        if(masyvas7.equals(masyvas8)) {
            System.out.println("Lygus");
        } else {
            System.out.println("Nelygus");
        }

        System.out.println("Uzduotis7: \n");
        //7
        int skaicius = 5;
        System.out.println(--skaicius);
        System.out.println(skaicius--);
        System.out.println(skaicius++);
        System.out.println(++skaicius);

        System.out.println("Uzduotis8: \n");
        //8
        String txt1 = "";
        String txt2 = "";

        if(txt1 == txt2) {
            System.out.println("Lygus");
        }

        System.out.println("Uzduotis9: \n");
        //9
        String txt3 = "";
        String txt4 = "abc";

        if(txt3.equals(txt4)) {
            System.out.println("Lygus");
        } else {
            System.out.println("Nelygus");
        }

        System.out.println("Uzduotis10: \n");
        //10
        String txt5 = "antras";
        System.out.println("1:" + "test");
        System.out.println("2:" + "test".trim());
        System.out.println("3:" + "test".trim().length());
        System.out.println("4:" + txt5.indexOf('a'));
        System.out.println("5:" + txt5.charAt(0));
        System.out.println("6:" + txt5.concat("123"));
        System.out.println("7:" + txt5.equals("antras"));
        System.out.println("8:" + txt5.equals(txt5));
        System.out.println("9:" + txt5.lastIndexOf('a'));

        System.out.println("Uzduotis11: \n");
        //11
        String txt6 = "tekstas";
        System.out.println("1:" + txt6.toUpperCase());
        System.out.println("2:" + txt6.toLowerCase());
        System.out.println("3:" + txt6.substring(0));
        System.out.println("4:" + txt6.substring(3,3));
        System.out.println("5:" + txt6.substring(3,4));
        System.out.println("5:" + txt6.startsWith("tek"));

   //     List<Integer> list = new ArrayList<>();
  //      Map<Integer, String> map = new HashMap<>();

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            list.add(3*i);
        }
        int iter=0;
        do{
            System.out.println(list.get(iter));
            iter++;
        }while (iter<list.size());

        iter=0;
        while (iter<list.size()){
            System.out.println(list.get(iter));
            iter++;
        }

        for (int j=0;j<list.size();j++){
            System.out.println(list.get(j));
        }

        for (Integer element : list){
            System.out.println(element);
        }

        List<String> tekstuSarasas= new ArrayList<>();
        tekstuSarasas.add("pirmasis");
        tekstuSarasas.add("antrasis");
        tekstuSarasas.add("karas ir taika");

        String[] tekstuMasyvas = new String[3];
        tekstuMasyvas[0] = "pirmasis";
        tekstuMasyvas[1] = "antrasis";
        tekstuMasyvas[2] = "karas ir taika";

        tekstuSarasas.add("labas");
        tekstuSarasas.add("ate");

        for (String masyvoElementas : tekstuMasyvas) {

        }

        for (String sarasoElementas : tekstuSarasas) {
            System.out.println(sarasoElementas);
        }

        Map<String, Integer> mapas = new HashMap<>();

        mapas.put("labas", 1);
        mapas.put("ate", 2);

        for (Map.Entry<String, Integer> element : mapas.entrySet())
        {
            System.out.println(element.getKey());
            System.out.println(element.getValue());
        }


        list.forEach(irasas -> {
            irasas.doubleValue();
        });
        list.stream()
                .filter(element -> element != 1)
                .collect(Collectors.toList());

        mapas.forEach((raktas, reiksme) -> {
            System.out.println(raktas);
            System.out.println(reiksme);
        });


    }
}
