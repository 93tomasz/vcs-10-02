package lt.vcs.tomas;

/**
 * Created by tomas on 2017-10-06.
 */

public class Skaiciavimai {
    public static void main(String[] args){
        toDaysHoursMinutes(2000);
    }
    public static boolean isEven(int number) {
        int liekana = number % 2;
        if (liekana == 0) return true;
        else return false;
    }

    public static int returnRemain (int number){
        return (number%10);
    }

    public static int invNumber(int number){
        int invNum=0;
        while(number>0){
            invNum=10*invNum+number%10;
            number=number/10;
        }
        return invNum;
    }

    public static void toDaysHoursMinutes(int minutes) {
        int days = minutes / 60 / 24;
        minutes -= days * 60 * 24;
        int hours = minutes / 60;
        minutes %= 60;

        System.out.println("Days: " + days);
        System.out.println("Hours: " + hours);
        System.out.println("Minutes: " + minutes);
    }


}
