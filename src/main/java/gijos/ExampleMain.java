package gijos;

import java.util.ArrayList;
import java.util.List;

public class ExampleMain {

    public static void main(String[] args) {
        List<Integer> sarasas;
        sarasas = new ArrayList<>();

        for (int i = 0; i < 500000; i++) {
            sarasas.add(i);
        }

        Thread thread1 = new MyThread(sarasas, 0, 249999);
        Thread thread2 = new MyThread(sarasas, 250000, 499999);
        Thread dideleGija = new DideleGija(sarasas, 0, 499999);

        dideleGija.start();
        thread1.start();
        thread2.start();
    }
}
