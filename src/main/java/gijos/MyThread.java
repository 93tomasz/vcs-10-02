package gijos;

import java.util.List;

public class MyThread extends Thread {


    private final List<Integer> sarasas;
    private final int nuo;
    private final int iki;

    public MyThread(List<Integer> sarasas, int nuo, int iki) {
        this.sarasas = sarasas;
        this.nuo = nuo;
        this.iki = iki;
    }

    @Override
    public void run(){
        long millis = System.currentTimeMillis();
        for(int i = nuo; i <= iki; i ++) {
            System.out.println("Gija " + this.sarasas.get(i));
        }
        millis = System.currentTimeMillis() - millis;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Gijos nuo " + nuo + " iki " + iki + " laikas " + (millis));

    }
}